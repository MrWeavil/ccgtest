﻿using UnityEngine;

namespace Cards
{
    public interface ICardView
    {
        Transform Transform { get; }
        BaseObject Obj { get; }
        CardViewState CardViewState { get; set; }
        CardModel Model { get; }
        void RedrawView();
    }
}