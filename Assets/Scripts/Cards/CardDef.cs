﻿namespace Cards
{
    public struct CardDef
    {
        public int Health;
        public int Damage;
        public string Description;
        public string Title;
        public string ImagePath;
    }
}