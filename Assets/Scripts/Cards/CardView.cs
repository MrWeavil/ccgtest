﻿using Controllers.TableController;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

namespace Cards
{
    public class CardView : BaseObject, ICardView, IDamageHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private string _cachedIconPath;

        private Sprite _cachedIconSprite;
        [SerializeField] private TextMeshProUGUI _damage;
        [SerializeField] private TextMeshProUGUI _description;
        [SerializeField] private TextMeshProUGUI _health;
        [SerializeField] private Image _icon;
        [SerializeField] private Canvas _canvas;

        [Inject] private IResourceLoader _resourceLoader;
        [SerializeField] private TextMeshProUGUI _title;

        [Inject] private ITableCardsController _tableCardsController;
        [Inject] private CameraController _cameraController;
        public Transform Transform => transform;
        public BaseObject Obj => this;
        public CardViewState CardViewState { get; set; }
        public CardModel Model { get; private set; }


        public void SetData(CardModel cardModel)
        {
            Model = cardModel;
            RedrawView();
        }

        public override void Init()
        {
            base.Init();
            _canvas.worldCamera = _cameraController.Camera;
        }

        public override void Uninit()
        {
            Model = null;
            _cachedIconPath = null;
            _cachedIconSprite = null;
            base.Uninit();
        }

        private Sprite GetIconSprite()
        {
            if (!string.Equals(Model.ImagePath, _cachedIconPath))
            {
                _cachedIconPath = Model.ImagePath;
                _cachedIconSprite = _resourceLoader.LoadResource<Sprite>(_cachedIconPath);
            }

            return _cachedIconSprite;
        }

        public void RedrawView()
        {
            _damage.text = Model.Damage.ToString();
            _health.text = Model.Health.ToString();
            _title.text = Model.Title;
            _description.text = Model.Description;
            _icon.sprite = GetIconSprite();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            DragAction(eventData);
            if (CardViewState == CardViewState.Active)
            {
                transform.rotation = Quaternion.identity;
                transform.rotation = Quaternion.AngleAxis(90f, transform.right);
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            DragAction(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            DragAction(eventData);
            if (CardViewState == CardViewState.Active)
            {
                var hitResult = new HitResult();
                if (Physics.Raycast(transform.position, Vector3.down, out var hit))
                {
                    hitResult.HasHit = true;
                    hitResult.Result = hit;
                    hitResult.CardView = this;
                };
                
                _tableCardsController.HandleHit(hitResult);
            }
        }
        
        private void DragAction(PointerEventData eventData)
        {
            if (CardViewState != CardViewState.Active)
            {
                return;
            }
            transform.position = _cameraController.GetPositionOnPlane(eventData.position);
        }

        public void TakeSimpleDamage(int damage)
        {
            Model.Health = Mathf.Max(0, Model.Health - 1);
            _tableCardsController.OnCardTakeDamage(this);
        }
    }
}