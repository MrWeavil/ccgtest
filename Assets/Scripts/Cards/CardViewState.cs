﻿namespace Cards
{
    public enum CardViewState
    {
        Active,
        Inactive,
        OnTable
    }
}