﻿namespace Cards
{
    public class CardModel
    {
        public int Damage;
        public string Description;
        public int Health;
        public string ImagePath;
        public string Title;
        public CardDef CardDef { get; private set; }


        public CardModel(CardDef cardDef)
        {
            CardDef = cardDef;
            RestoreModel();
        }

        public void RestoreModel()
        {
            Health = CardDef.Health;
            Damage = CardDef.Damage;
            Description = CardDef.Description;
            Title = CardDef.Title;
            ImagePath = CardDef.ImagePath;
        }
    }
}