﻿using System.Collections.Generic;

namespace Cards
{
    public class CardStorage
    {
        private List<CardDef> _cards = new List<CardDef>();

        public CardStorage()
        {
            InitCardDefs();
        }

        public List<CardDef> GetRandomCardDefs(int elementsCount)
        {
            return _cards.GetRandomElements(elementsCount);
        }

        private void InitCardDefs()
        {
            _cards = new List<CardDef>();
            _cards.Add(new CardDef
            {
                Health = 1,
                Damage = 3,
                ImagePath = "Images/1",
                Title = "Title 1",
                Description = "Lorem ipsum dolor <b>sit amet, consectetur adipiscing</b> elit, " +
                              "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim v"
            });
            _cards.Add(new CardDef
            {
                Health = 2,
                Damage = 9,
                ImagePath = "Images/2",
                Title = "Title 2",
                Description = "Lorem ipsum dolor <b>sit amet adipiscing</b> elit, " +
                              "sed do eiusmod tempor ut labore et dolore magna aliqua. Ut enim ad minim v"
            });
            _cards.Add(new CardDef
            {
                Health = 9,
                Damage = 1,
                ImagePath = "Images/3",
                Title = "Title 3",
                Description = "Lorem consectetur adipiscing</b> elit, " +
                              "sed  et dolore magna aliqua. Ut enim ad minim v"
            });
            _cards.Add(new CardDef
            {
                Health = 5,
                Damage = 5,
                ImagePath = "Images/4",
                Title = "Title 4",
                Description =
                    " Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            });
            _cards.Add(new CardDef
            {
                Health = 9,
                Damage = 9,
                ImagePath = "Images/5",
                Title = "Title 5",
                Description =
                    " Excepteur anim id est laborum."
            });
            _cards.Add(new CardDef
            {
                Health = 1,
                Damage = 1,
                ImagePath = "Images/6",
                Title = "Title 6",
                Description =
                    "Aliquam sem et tortor consequat id porta nibh venenatis cras. " +
                    "Turpis massa sed elementum tempus. Lacinia quis vel eros donec ac. Elit" +
                    " sed vulputate mi sit amet mauris commodo quis imperdiet. Justo donec enim diam vulputate ut pharetra."
            });
            _cards.Add(new CardDef
            {
                Health = 3,
                Damage = 8,
                ImagePath = "Images/7",
                Title = "Title 7",
                Description =
                    "Turpis massa sed elementum tempus. Lacinia quis vel eros donec ac. Elit" +
                    " sed vulputate mi sit amet mauris commodo quis imperdiet. Justo donec enim diam vulputate ut pharetra."
            });
            _cards.Add(new CardDef
            {
                Health = 2,
                Damage = 7,
                ImagePath = "Images/8",
                Title = "Title 8",
                Description =
                    "Turpis massa sed elementum tempus. Lacinia quis vel eros donec ac. Elit" +
                    " sed ut pharetra."
            });
            _cards.Add(new CardDef
            {
                Health = 2,
                Damage = 2,
                ImagePath = "Images/9",
                Title = "Title 9",
                Description =
                    "Turpis massa sed elementum tempus. Lacinia quis vel eros donec ac. Elit"
            });
            _cards.Add(new CardDef
            {
                Health = 8,
                Damage = 8,
                ImagePath = "Images/10",
                Title = "Title 10",
                Description =
                    "Nunc aliquet bibendum enim facilisis gravida. Tincidunt ornare massa eget egestas purus viverra"
            });
            _cards.Add(new CardDef
            {
                Health = 1,
                Damage = 6,
                ImagePath = "Images/11",
                Title = "Title 11",
                Description =
                    "Nunc aliquet bibendum enim facilisis gravida. Tincidunt ornare massa eget egestas purus viverra"
            });
            _cards.Add(new CardDef
            {
                Health = 1,
                Damage = 9,
                ImagePath = "Images/12",
                Title = "Title 12",
                Description =
                    "Diam donec adipiscing tristique risus nec. Imperdiet proin fermentum " +
                    "leo vel orci. Risus in hendrerit gravida rutrum quisque. Nisl nisi scelerisque eu ultrices vitae auctor eu." +
                    " Sagittis orci a scelerisque purus semper eget duis. Tortor pretiu"
            });
            _cards.Add(new CardDef
            {
                Health = 9,
                Damage = 9,
                ImagePath = "Images/13",
                Title = "Title 13",
                Description =
                    "Diam donec adipiscing tristique risus nec. Imperdiet proin fermentum " +
                    "leo vel orci. Risus in " +
                    " Sagittis orci a scelerisque purus semper eget duis. Tortor pretiu"
            });
            _cards.Add(new CardDef
            {
                Health = 9,
                Damage = 9,
                ImagePath = "Images/14",
                Title = "Title 14",
                Description =
                    "Diam donec adipiscing tristique risus nec. Imperdiet Sagittis orci a scelerisque purus semper eget duis. Tortor pretiu"
            });
            _cards.Add(new CardDef
            {
                Health = 9,
                Damage = 9,
                ImagePath = "Images/15",
                Title = "Title 15",
                Description =
                    "Diam donec adipiscing tristique risus nec."
            });
        }
    }
}