﻿using UnityEngine;


public interface IResourceLoader
{
    T LoadResource<T>(string path) where T : Object;
}