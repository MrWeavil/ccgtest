﻿using UnityEngine;

public class SimpleResourceLoader : IResourceLoader
{
    public T LoadResource<T>(string path) where T : Object
    {
        var resource = Resources.Load<T>(path);
        if (resource == null)
            throw new UnityException(string.Format("Wrong path: {0}", path));

        return resource;
    }
}