﻿using Cards;
using Controllers.PlayerDeckController;
using Controllers.PlayerHandController;
using UnityEngine;
using Zenject;

public class SceneInitializer : MonoBehaviour
{
    [Inject] private CardStorage _cardStorage;
    [Inject] private IPlayerDeckController _deckController;
    [Inject] private IPlayerHandController _playerHandController;
    [Inject] private IPool _pool;


    private void Start()
    {
        InitializeScene();
    }

    private void InitializeScene()
    {
        var randomCardDefs = _cardStorage.GetRandomCardDefs(12);
        for (var i = 0; i < randomCardDefs.Count; i++)
        {
            var cardView = _pool.Pull<CardView>("Prefabs/CardView");
            cardView.SetData(new CardModel(randomCardDefs[i]));
            var result = _deckController.Push(cardView);

            if (result.Status == DeckOperationStatus.Full) _playerHandController.Push(result.CardView);
        }
    }
}