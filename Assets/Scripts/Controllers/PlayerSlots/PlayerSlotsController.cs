﻿using System.Collections.Generic;
using System.Linq;
using Cards;
using UnityEngine;

namespace Controllers.PlayerSlots
{
    public class PlayerSlotsController : MonoBehaviour, IPlayerSlotsController
    {
        [SerializeField] private List<TablePlayerSlot>  _slots;
    
        public void MakeDamageToAll(int damage)
        {
            _slots.ForEach(slot => slot.MakeDamage(damage));
        }

        public List<ICardView> GetCards()
        {
            return new List<ICardView>(_slots.Select(s => s.CardView).Where(c => c != null));
        }

        public void ClearAll()
        {
            _slots.ForEach(s => s.ClearSlot());
        }
    
        public void ClearSlot(ICardView cardView)
        {
            _slots.ForEach(slot => slot.TryClearSlot(cardView));
        }
    }
}