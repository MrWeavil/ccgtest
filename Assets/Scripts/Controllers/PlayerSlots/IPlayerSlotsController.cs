﻿using System.Collections.Generic;
using Cards;

namespace Controllers.PlayerSlots
{
    public interface IPlayerSlotsController
    {
        void MakeDamageToAll(int damage);
        List<ICardView> GetCards();
        void ClearAll();
        void ClearSlot(ICardView cardView);
    }
}