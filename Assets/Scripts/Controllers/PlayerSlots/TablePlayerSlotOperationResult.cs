﻿namespace Controllers.PlayerSlots
{
    public enum TablePlayerSlotOperationResult
    {
        Done,
        Busy,
        Empty,
        NotEquals
    }
}