﻿using Cards;
using Controllers.TableController;
using UnityEngine;

namespace Controllers.PlayerSlots
{
    public class TablePlayerSlot : BaseObject
    {
        private IDamageHandler _cardDamageHandler;
        [SerializeField] private Transform _cardPivot;
        public ICardView CardView { get; private set; }

        public TablePlayerSlotOperationResult SetCardView(ICardView cardView)
        {
            if (CardView != null) return TablePlayerSlotOperationResult.Busy;
            CardView = cardView;
            _cardDamageHandler = cardView as IDamageHandler;
            CardView.Transform.SetParent(_cardPivot);
            cardView.Transform.localPosition = Vector3.zero;
            cardView.Transform.localRotation = Quaternion.identity;
            cardView.CardViewState = CardViewState.OnTable;
            return TablePlayerSlotOperationResult.Done;
        }

        public TablePlayerSlotOperationResult SlotStatus()
        {
            return CardView == null ? TablePlayerSlotOperationResult.Empty : TablePlayerSlotOperationResult.Busy;
        }

        public TablePlayerSlotOperationResult ClearSlot()
        {
            if (CardView == null) return TablePlayerSlotOperationResult.Empty;

            CardView = null;
            _cardDamageHandler = null;
            return TablePlayerSlotOperationResult.Done;
        }

        public TablePlayerSlotOperationResult TryClearSlot(ICardView cardView)
        {
            if (CardView == cardView)
            {
                ClearSlot();
                return TablePlayerSlotOperationResult.Done;
            }

            return TablePlayerSlotOperationResult.NotEquals;
        }

        public void MakeDamage(int damage)
        {
            _cardDamageHandler?.TakeSimpleDamage(damage);
        }
    }
}