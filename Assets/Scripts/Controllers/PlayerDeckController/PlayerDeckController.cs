﻿using System.Collections.Generic;
using Cards;
using UnityEngine;

namespace Controllers.PlayerDeckController
{
    public class PlayerDeckController : MonoBehaviour, IPlayerDeckController
    {
        private const float _distanceBetweenCards = 0.24f;
        private readonly Stack<ICardView> _cardViews = new Stack<ICardView>();
        [SerializeField] private Transform _deckStartPivot;

        public DeckOperationResult Push(ICardView cardView)
        {
            if (_cardViews.Count >= Constants.MaximumDeckCapacity)
                return new DeckOperationResult(DeckOperationStatus.Full, cardView);

            _cardViews.Push(cardView);
            cardView.Transform.SetParent(transform);
            cardView.CardViewState = CardViewState.Inactive;
            RedrawViews();
            return new DeckOperationResult(DeckOperationStatus.Done, cardView);
        }

        public DeckOperationResult Pull()
        {
            if (_cardViews.Count == 0) return new DeckOperationResult(DeckOperationStatus.Empty, null);

            var cardView = _cardViews.Pop();
            RedrawViews();
            return new DeckOperationResult(DeckOperationStatus.Done, cardView);
        }

        public void RedrawViews()
        {
            var cardDistanceModifier = 0;
            foreach (var cardView in _cardViews)
            {
                cardView.Transform.localRotation = _deckStartPivot.localRotation;
                cardView.Transform.position = _deckStartPivot.position +
                                              _deckStartPivot.forward * -1f * cardDistanceModifier *
                                              _distanceBetweenCards;
                cardDistanceModifier++;
            }
        }
    }
}