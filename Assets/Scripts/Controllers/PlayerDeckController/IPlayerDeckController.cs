﻿using Cards;

namespace Controllers.PlayerDeckController
{
    public interface IPlayerDeckController
    {
        DeckOperationResult Push(ICardView cardView);
        DeckOperationResult Pull();
    }
}