﻿namespace Controllers.PlayerDeckController
{
    public enum DeckOperationStatus
    {
        Done,
        Empty,
        Full
    }
}