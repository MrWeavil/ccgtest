﻿using Cards;

namespace Controllers.PlayerDeckController
{
    public class DeckOperationResult
    {
        public DeckOperationStatus Status;
        public ICardView CardView;

        public DeckOperationResult(DeckOperationStatus status, ICardView cardView)
        {
            Status = status;
            CardView = cardView;
        }
    }
}