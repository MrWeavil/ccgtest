﻿namespace Controllers.TableController
{
    public interface IDamageHandler
    {
        void TakeSimpleDamage(int damage);
    }
}