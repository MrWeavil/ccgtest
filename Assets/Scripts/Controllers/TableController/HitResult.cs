﻿using Cards;
using UnityEngine;

namespace Controllers.TableController
{
    public class HitResult
    {
        public bool HasHit;
        public RaycastHit Result;
        public ICardView CardView;
    }
}