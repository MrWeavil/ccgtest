﻿using Cards;
using Controllers.PlayerDeckController;
using Controllers.PlayerHandController;
using Controllers.PlayerSlots;
using Zenject;

namespace Controllers.TableController
{
    public class TableCardsController : ITableCardsController
    {
        [Inject] private IPool _pool;
        [Inject] private IPlayerDeckController _playerDeckController;
        [Inject] private IPlayerHandController _playerHandController;
        [Inject] private IPlayerSlotsController _playerSlotsController;

        public void HandleHit(HitResult hit)
        {
            if (!hit.HasHit)
            {
                _playerHandController.RedrawViews();
            }

            var slot = hit.Result.collider.gameObject.GetComponent<TablePlayerSlot>();
            if (slot?.SlotStatus() == TablePlayerSlotOperationResult.Empty)
            {
                slot.SetCardView(hit.CardView);
                _playerHandController.RemoveCardView(hit.CardView);
            }
            else
            {
                _playerHandController.RedrawViews();
            }
        }

        public bool IsDeath(ICardView cardView)
        {
            if (cardView.Model.Health <= 0)
            {
                return true;
            }

            return false;
        }

        public void OnCardTakeDamage(ICardView cardView)
        {
            cardView.RedrawView();
            if (IsDeath(cardView))
            {
                _playerSlotsController.ClearSlot(cardView);
                _pool.Push(cardView.Obj);
            }
        }

        public void MakeDamageToAll(int damage)
        {
            _playerSlotsController.MakeDamageToAll(damage);
        }

        public void ReturnAllCards()
        {
            var cards = _playerSlotsController.GetCards();
            foreach (var card in cards)
            {
                card.Model.RestoreModel();
                card.RedrawView();
                var result = _playerDeckController.Push(card);
                if (result.Status != DeckOperationStatus.Done)
                {
                    if (_playerHandController.Push(card).Status != HandOperionStatus.Done)
                    {
                        _pool.Push(card.Obj);
                    }
                }
            }
            _playerSlotsController.ClearAll();
        }

        public void GetCard()
        {
            if (_playerHandController.CardsCount == Constants.MaximumHandCapacity)
            {
                return;
            }
            var result = _playerDeckController.Pull();
            if (result.Status != DeckOperationStatus.Done) return;
            _playerHandController.Push(result.CardView);
        }
    }
}