﻿using Cards;

namespace Controllers.TableController
{
    public interface ITableCardsController
    {
        void HandleHit(HitResult hit);
        bool IsDeath(ICardView cardView);
        void OnCardTakeDamage(ICardView cardView);
        void MakeDamageToAll(int damage);
        void ReturnAllCards();
        void GetCard();
    }
}