﻿using System.Collections.Generic;
using Cards;
using UnityEngine;

namespace Controllers.PlayerHandController
{
    public class PlayerHandController : MonoBehaviour, IPlayerHandController
    {
        private readonly List<ICardView> _cardViews = new List<ICardView>();
        [SerializeField] private List<Transform> _pivots;


        public HandOperationResult RemoveCardView(ICardView cardView)
        {
            var cardIndex = _cardViews.FindIndex(c => c == cardView);
            if (cardIndex == -1) return new HandOperationResult(HandOperionStatus.NotExist, cardView);

            _cardViews.RemoveAt(cardIndex);
            RedrawViews();
            return new HandOperationResult(HandOperionStatus.Done, cardView);
        }

        public HandOperationResult Push(ICardView cardView)
        {
            if (_cardViews.Count >= Constants.MaximumHandCapacity)
                return new HandOperationResult(HandOperionStatus.Full, cardView);

            _cardViews.Add(cardView);
            cardView.CardViewState = CardViewState.Active;
            RedrawViews();
            return new HandOperationResult(HandOperionStatus.Done, cardView);
        }

        public int CardsCount => _cardViews?.Count ?? 0;

        private void OnValidate()
        {
            if (_pivots.Count != Constants.MaximumHandCapacity)
                Debug.LogError(string.Format("Pivots count not equal {0}", Constants.MaximumHandCapacity));
        }

        
        public void RedrawViews()
        {
            for (var i = 0; i < _cardViews.Count; i++)
            {
                var cardView = _cardViews[i];
                var pivot = _pivots[i];

                cardView.Transform.SetParent(pivot);
                cardView.Transform.localRotation = Quaternion.identity;
                cardView.Transform.localPosition = Vector3.zero;
            }
        }
    }
}