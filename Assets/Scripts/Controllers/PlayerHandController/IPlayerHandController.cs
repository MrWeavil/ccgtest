﻿using Cards;

namespace Controllers.PlayerHandController
{
    public interface IPlayerHandController
    {
        HandOperationResult RemoveCardView(ICardView cardView);
        HandOperationResult Push(ICardView cardView);
        int CardsCount { get; }
        void RedrawViews();
    }
}