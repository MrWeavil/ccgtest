﻿using Cards;

namespace Controllers.PlayerHandController
{
    public class HandOperationResult
    {
        public ICardView CardView;
        public HandOperionStatus Status;

        public HandOperationResult(HandOperionStatus status, ICardView cardView)
        {
            Status = status;
            CardView = cardView;
        }
    }
}