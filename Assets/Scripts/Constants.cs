﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const int MaximumDeckCapacity = 7;
    public const int MaximumHandCapacity = 5;
}
