﻿
using Controllers.TableController;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class SimpleUI : MonoBehaviour
{
    [Inject] private ITableCardsController _tableCardsController;
    
    [SerializeField] private Button _returnAllCardsButton;
    [SerializeField] private Button _getCardButton;
    [SerializeField] private Button _damageButton;

    void Start()
    {
        _returnAllCardsButton.onClick.AddListener(() => { _tableCardsController.ReturnAllCards();});
        _getCardButton.onClick.AddListener(() => { _tableCardsController.GetCard();});
        _damageButton.onClick.AddListener(() => { _tableCardsController.MakeDamageToAll(1);});
    }
}