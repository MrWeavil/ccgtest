﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _planeCenter;
    [SerializeField] private Camera _camera;
    public Camera Camera => _camera;
    
    
    //Считает пересечение луча, параллельного направлению камеры, с плоскостью, заланной переменной _planeCenter
    public Vector3 GetPositionOnPlane(Vector2 screenPosition)
    {
        var nearClipPlanePosition = GetNearClipPlanePosition(screenPosition);
        var farClipPlanePosition  = GetFarClipPlanePosition(screenPosition);
        
        var targetPlane = new Plane(_planeCenter.up, _planeCenter.position);

        var rayDirection = farClipPlanePosition - nearClipPlanePosition;
        if (targetPlane.Raycast(new Ray(nearClipPlanePosition, rayDirection),
            out var distanceToHitTargetPlane))
        {
            return nearClipPlanePosition + (rayDirection.normalized * distanceToHitTargetPlane);
        }
        
        return Vector3.zero;
    }

    Vector3 GetNearClipPlanePosition(Vector2 screenPosition)
    {
        var point = new Vector3(screenPosition.x, screenPosition.y, Camera.nearClipPlane);
        return Camera.ScreenToWorldPoint(point);
    }

    Vector3 GetFarClipPlanePosition(Vector3 screenPosition)
    {
        var point = new Vector3(screenPosition.x, screenPosition.y, Camera.farClipPlane);
        return Camera.ScreenToWorldPoint(point);
    }
}