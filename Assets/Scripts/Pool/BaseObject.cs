﻿using UnityEngine;
using Zenject;

public class BaseObject : MonoBehaviour, IPoolObject
{
    public virtual void Init()
    {
    }

    public virtual void Uninit()
    {
    }

    public GameObject GameObject => gameObject;
}