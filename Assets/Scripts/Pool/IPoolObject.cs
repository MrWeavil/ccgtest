﻿using UnityEngine;

public interface IPoolObject
{
    void Init();
    void Uninit();
    GameObject GameObject { get; }
}