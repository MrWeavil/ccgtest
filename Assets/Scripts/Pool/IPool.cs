﻿using UnityEngine;

public interface IPool
{
    void Push(IPoolObject obj);
    T Pull<T>(string path) where T : Object;
}