﻿using UnityEngine;
using Zenject;

public class FakePool : IPool
{
    [Inject] private IResourceLoader _resourceLoader;

    public void Push(IPoolObject obj)
    {
        obj.Uninit();
        Object.Destroy(obj.GameObject);
    }

    public T Pull<T>(string path) where T : Object
    {
        var val = Object.Instantiate(_resourceLoader.LoadResource<T>(path));
        (val as IPoolObject)?.Init();
        return val;
    }
}