﻿using Cards;
using Controllers.PlayerDeckController;
using Controllers.PlayerHandController;
using Controllers.PlayerSlots;
using Controllers.TableController;
using UnityEngine;
using Zenject;

public class ContextInstaller : MonoInstaller
{
    [SerializeField] private CameraController _cameraController;
    [SerializeField] private PlayerHandController _playerHandController;
    [SerializeField] private PlayerSlotsController _playerSlotsController;
    [SerializeField] private PlayerDeckController playerDeckController;

    public override void InstallBindings()
    {
        Container.Bind<IResourceLoader>().To<SimpleResourceLoader>().AsSingle();
        Container.Bind<IPool>().To<FakePool>().AsSingle();
        Container.Bind<IPlayerDeckController>().To<PlayerDeckController>().FromInstance(playerDeckController)
            .AsSingle();
        Container.Bind<IPlayerHandController>().To<PlayerHandController>().FromInstance(_playerHandController)
            .AsSingle();
        Container.Bind<ITableCardsController>().To<TableCardsController>().AsSingle();
        Container.Bind<IPlayerSlotsController>().To<PlayerSlotsController>().FromInstance(_playerSlotsController)
            .AsSingle();

        Container.BindInstance(_cameraController);
        Container.BindInstance(new CardStorage());
    }
}