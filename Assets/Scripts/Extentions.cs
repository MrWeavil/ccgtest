﻿using System.Collections.Generic;
using UnityEngine;

public static class Extentions
{
    public static List<T> GetRandomElements<T>(this IEnumerable<T> list, int elementsCount)
    {
        var listCopy = new List<T>();
        var values = new List<T>();

        foreach (var card in list) listCopy.Add(card);

        elementsCount = Mathf.Clamp(elementsCount, 0, listCopy.Count);

        for (var i = 0; i < elementsCount; i++)
        {
            var randomIndex = Random.Range(0, listCopy.Count);
            values.Add(listCopy[randomIndex]);
            listCopy.RemoveAt(randomIndex);
        }

        return values;
    }
}